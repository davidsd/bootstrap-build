{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DerivingStrategies   #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE UndecidableInstances #-}

-- | This module defines 'runMemoFetchT', which is a version of
-- 'runFetchT' that fetches a given key no more than once by memoizing
-- the fetch function, and furthermore frees values from memory when
-- they are no longer needed.

module Bootstrap.Build.MemoFetch
  ( runMemoFetchT
  , MemoFetchT
  , GetDependencies
  , MemoFetchAll
  ) where

import Bootstrap.Build.Fetches    (FetchConfig (..), FetchT, Fetches (..),
                                   FetchesAll, runFetchT)
import Bootstrap.Build.FList      (FList (..), zipWithF)
import Bootstrap.Build.HasForce   (HasForce (..))
import Bootstrap.Build.KeyVal     (HasKeyVal (..), KeyValElem (..))
import Bootstrap.Build.TypeUtils  (All, AllF, Keys)
import Control.Monad.IO.Class     (MonadIO)
import Control.Monad.State.Strict (MonadState, StateT, evalStateT, get, lift,
                                   modify')
import Data.Constraint            (Dict (..))
import Data.Functor.Const         (Const (..))
import Data.Kind                  (Type)
import Data.Map.Strict            (Map)
import Data.Map.Strict            qualified as Map

------------ KVMaps ------------

-- | A map from keys 'k' to a pair (occur, mv). Here, 'occur' is a
-- positive Int representing the number of times the key k will be
-- encountered in the rest of the computation. Meanwhile mv contains
-- the result of 'fetch k' if it has already been computed, and Maybe
-- otherwise.
type KVMap k v = Map k (Int, Maybe v)

-- | Given a list of keys in the order that they will be encountered
-- in a computation, build a KV map containing the number of times
-- each key occurs, together with 'Nothing' to represent that none of
-- the keys have been fetched yet.
emptyKVMap :: Ord k => [k] -> KVMap k v
emptyKVMap ks =
  fmap (\n -> (n, Nothing)) $
  Map.fromListWith (+) (zip ks (repeat @Int 1))

-- | A heterogeneous collection of KVMap's
data KVMaps (kvs :: [(Type, Type)]) where
  KVCons :: KVMap k v -> KVMaps kvs -> KVMaps ( '(k,v) ': kvs )
  KVNil  :: KVMaps '[]

-- | Extract the appropriate KVMap from a KVMaps collection, given
-- 'KeyValElem k v kvs' as an index.
findKVMap :: KeyValElem k v kvs -> KVMaps kvs -> KVMap k v
findKVMap _         KVNil         = error "absurd"
findKVMap Here      (KVCons m _)  = m
findKVMap (There i) (KVCons _ ms) = findKVMap i ms

-- | Map a function over a particular KVMap in a KVMaps collection,
-- given 'KeyValElem k v' as an index.
mapKVMap :: KeyValElem k v kvs -> (KVMap k v -> KVMap k v) -> KVMaps kvs -> KVMaps kvs
mapKVMap _         _ KVNil          = error "absurd"
mapKVMap Here      f (KVCons m ms)  = KVCons (f m) ms
mapKVMap (There i) f (KVCons m' ms) = KVCons m' (mapKVMap i f ms)

-- | Create a collection of empty VM maps with the appropriate
-- occurrence counts.
emptyKVMaps :: All Ord (Keys kvs) => FetchConfig m kvs -> FList [] (Keys kvs) -> KVMaps kvs
emptyKVMaps FetchNil FNil = KVNil
emptyKVMaps (_ :&: cfg) (ks ::: rest) = emptyKVMap ks `KVCons` emptyKVMaps cfg rest

------------ MemoFetchT ------------

-- | FetchT, enriched with KVMaps that keep track of the results of
-- each fetch only for as long as it is needed. We do this by first
-- pre-computing the number of times each key is fetched, and building
-- those occurences into the KVMaps. Each time we fetch a key, we
-- decrement its occurence count, and when the count reaches 0, we
-- remove the key-value pair from the map, freeing up its memory.
--
-- Note that MemoFetchT is unsafe to use outside of 'runMemoFetchT',
-- since it will throw errors if the occurence counts are not correct.
newtype MemoFetchT kvs m a =
  MkMemoFetchT { unMemoFetchT :: StateT (KVMaps kvs) (FetchT kvs m) a }
  deriving newtype
    ( Functor
    , Applicative
    , Monad
    , MonadState (KVMaps kvs)
    , MonadIO
    )
  deriving anyclass (HasForce)

instance (Ord k, HasKeyVal k v kvs, Monad m) => Fetches k v (MemoFetchT kvs m) where
  fetch key = do
    kvMap <- findKVMap keyValElem <$> get
    case Map.lookup key kvMap of
      Nothing -> error "MemoFetchT: Unexpected key encountered"
      Just (count, mv) -> do
        v <- case mv of
          Just v  -> pure v
          Nothing -> MkMemoFetchT $ lift $ fetch key
        let newCount = count - 1
        modify' $ mapKVMap keyValElem $
          if newCount <= 0
          then Map.delete key
          else Map.insert key (newCount, Just v)
        pure v

------------ MultiList ------------

-- | A list of lists [ks_1, ks_2, ...], with ks_i :: [t_i], where the
-- types t_1,t_2,... can be different. The newtype exists to allow us
-- to define a Semigroup and Monoid instance, which are needed in
-- 'GetDependencies'.
newtype MultiList ks = MkMultiList (FList [] ks)

instance Semigroup (MultiList ks) where
  MkMultiList xs <> MkMultiList ys = MkMultiList (zipWithF (<>) xs ys)

-- | A typeclass for constructing an empty FList [] ks, needed for the
-- Monoid instance. Note that we can construct a 'Dict' for
-- 'HasEmptyF' using any concrete data built from the type-level list
-- 'ks'. Below, we do it using a 'FetchConfig'.
class HasEmptyF (ks :: [Type]) where
  emptyF :: FList [] ks

instance HasEmptyF '[] where
  emptyF = FNil

instance HasEmptyF ks => HasEmptyF (k ': ks) where
  emptyF = [] ::: emptyF

instance HasEmptyF ks => Monoid (MultiList ks) where
  mempty = MkMultiList emptyF

-- | Cons a single element onto a MultiList at the appropriate
-- location, using KeyValueElem k v as the outer index.
consMultiList :: KeyValElem k v kvs -> k -> MultiList (Keys kvs) -> MultiList (Keys kvs)
consMultiList i k (MkMultiList ml) = MkMultiList (go i k ml)
  where
    go :: KeyValElem k v kvs -> k -> FList [] (Keys kvs) -> FList [] (Keys kvs)
    go _         _ FNil          = error "absurd"
    go Here      x (xs ::: rest) = (x : xs) ::: rest
    go (There e) x (xs ::: rest) = xs ::: go e x rest

-- | A MultiList containing a single element of type k
singletonMultiList :: HasEmptyF (Keys kvs) => KeyValElem k v kvs -> k -> MultiList (Keys kvs)
singletonMultiList i k = consMultiList i k mempty

------------ GetDependencies ------------

-- | An Applicative to compute the dependencies of a Fetch computation
-- as a MultiList. This is essentially the same as
-- 'ComputeDependencies', but it returns the result in a slightly more
-- useful form for 'runMemoFetchT'.
newtype GetDependencies kvs a = MkGetDependencies (Const (MultiList (Keys kvs)) a)
  deriving newtype (Functor)

deriving newtype instance HasEmptyF (Keys kvs) => Applicative (GetDependencies kvs)

instance (HasEmptyF (Keys kvs), HasKeyVal k v kvs) => Fetches k v (GetDependencies kvs) where
  fetch key = MkGetDependencies (Const (singletonMultiList (keyValElem @k @v @kvs) key))

instance HasForce (GetDependencies kvs) where
  forceM = id

getDependencies :: GetDependencies kvs a -> FList [] (Keys kvs)
getDependencies (MkGetDependencies (Const (MkMultiList ml))) = ml

------------ runMemoFetchT ------------

-- | Given a FetchConfig m kvs, produce evidence that we can construct
-- an empty FList of keys. Pattern matching on this dictionary allows
-- us to use the Monoid instance for 'MultiList (Keys kvs)'.
witnessHasEmptyF :: FetchConfig m kvs -> Dict (HasEmptyF (Keys kvs))
witnessHasEmptyF FetchNil = Dict
witnessHasEmptyF (_ :&: cfg) = case witnessHasEmptyF cfg of
  Dict -> Dict

type MemoFetchAll kvs m =
  ( FetchesAll kvs (MemoFetchT kvs m)
  , FetchesAll kvs (GetDependencies kvs)
  , AllF Ord (Keys kvs)
  )

-- | Run a fetch computation, ensuring that no key is fetched more
-- than once, and each value is freed from memory when it is no longer
-- needed.
runMemoFetchT
  :: forall kvs m r . (MemoFetchAll kvs m, Monad m)
  => (forall f . (Applicative f, FetchesAll kvs f, HasForce f) => f r)
  -> FetchConfig m kvs
  -> m r
runMemoFetchT goFetch cfg =
  case witnessHasEmptyF cfg of
    Dict ->
      let
        kvMaps = emptyKVMaps cfg (getDependencies @kvs goFetch)
      in
        runFetchT (evalStateT (unMemoFetchT goFetch) kvMaps) cfg
