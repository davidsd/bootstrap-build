{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE GADTs        #-}
{-# LANGUAGE TypeFamilies #-}

module Bootstrap.Build.KeyVal where

import Bootstrap.Build.TypeUtils (Keys, Sum)

-- | A value of type 'KeyValElem k v kvs' is a proof that the
-- type-level lookup list kvs contains the pair (k,v).
data KeyValElem k v kvs where
  Here :: KeyValElem k v ('(k, v) ': kvs)
  There :: KeyValElem k v kvs -> KeyValElem k v ('(x, y) ': kvs)

-- | A typeclass that provides evidence for @(k,v)@ existing in @kvs@
-- We will get overlapping instances if we try to deduce
-- @'HasKeyVal' k v kvs@ for a @kvs@ in which @(k,v)@ appears more than
-- once.
class HasKeyVal k v kvs where
  keyValElem :: KeyValElem k v kvs

-- We need the OVERLAPPING pragma because GHC doesn't take context into
-- account when matching constraints. In such recursive definitions we
-- need the base of the recursion to be OVERLAPPING so that it is preferred
-- to the recursion step when we reach it.
instance {-# OVERLAPPING #-} HasKeyVal k v ('(k, v) ': kvs) where
  keyValElem = Here

instance HasKeyVal k v kvs => HasKeyVal k v ('(k', v') ': kvs) where
  keyValElem = There keyValElem

-- | Given evidence that (k,v) is in the environment kvs, and a key of
-- type k, apply nested 'Left's and 'Right's to get an element of
-- 'SumKeys kvs'.
toSumKeys :: forall k v kvs. KeyValElem k v kvs -> k -> Sum (Keys kvs)
toSumKeys Here k      = Left k
toSumKeys (There e) k = Right (toSumKeys e k)
