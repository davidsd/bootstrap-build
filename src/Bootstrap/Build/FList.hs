{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE GADTs        #-}
{-# LANGUAGE TypeFamilies #-}

module Bootstrap.Build.FList where

import Data.Functor.Const (Const (..))
import Data.Kind          (Type)

-- | A heterogeneous list, where the elements are given by applying a
-- functor 'f' to each type. Conceptually, this is essentially 'HList
-- (TypeMap f ks)', where 'TypeMap' is some type-level map function.
--
-- The letter 'F' comes from the presence of the functor 'f'. I am
-- open to a better name.
data FList (f :: Type -> Type) (ks :: [Type]) where
  -- | The 0-element FList
  FNil :: FList f '[]
  -- | The cons operation for FLists
  (:::) :: f k -> FList f ks -> FList f (k ': ks)

infixr 5 :::

-- | Map a natural transformation (f ~> g) over an FList
mapF :: (forall a . f a -> g a) -> FList f ks -> FList g ks
mapF _ FNil       = FNil
mapF f (x ::: xs) = f x ::: mapF f xs

-- | Zip two FLists using a natural transformation from (f, g) to h
zipWithF :: (forall a . f a -> g a -> h a) -> FList f ks -> FList g ks -> FList h ks
zipWithF _ FNil FNil             = FNil
zipWithF f (x ::: xs) (y ::: ys) = f x y ::: zipWithF f xs ys

-- | Transform an 'FList (Const a)' into a homogeneous list.
forgetF :: FList (Const a) ks -> [a]
forgetF FNil       = []
forgetF (x ::: xs) = getConst x : forgetF xs
