{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE QuantifiedConstraints #-}

module Bootstrap.Build.HasForce
  ( HasForce(..)
  ) where

import Control.DeepSeq      (NFData, deepseq)
import Data.Functor.Compose (Compose (..))
import Data.Tagged          (Tagged (..))

-- | A context in which we can deeply evaluate data before proceeding
-- to the rest of the computation. 'HasForce' is true of any Monad,
-- but we also provide dummy instances for 'ComputeDependencies'.
class HasForce m where
  forceM :: NFData a => m a -> m a

  default forceM :: (Monad m, NFData a) => m a -> m a
  forceM mx = do
    x <- mx
    x `deepseq` pure x

instance HasForce IO


instance {-# OVERLAPPABLE #-} (forall a . NFData a => NFData (g a), HasForce f) => HasForce (Compose f g) where
  forceM (Compose x) = Compose (forceM x)

instance HasForce m => HasForce (Compose (Tagged p) m) where
  forceM (Compose (Tagged x)) = Compose (Tagged (forceM x))
