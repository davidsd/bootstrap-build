{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE GADTs        #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}

module Bootstrap.Build.Fetches where

import Bootstrap.Build.HasForce  (HasForce(..))
import Bootstrap.Build.KeyVal    (HasKeyVal (..), KeyValElem (..), toSumKeys)
import Bootstrap.Build.TypeUtils (DropVoid (..), Keys, Sum)
import Control.Monad.IO.Class    (MonadIO)
import Control.Monad.Reader      (ReaderT (..))
import Data.Functor.Const        (Const (..))
import Data.Kind                 (Constraint, Type)

-- | Typeclass that describes a context which contains a @fetch@ function.
class Fetches k v m where
  fetch :: k -> m v

-- | To run a build task, we must supply a fetching function
-- for each 'Fetches m k v' instance that it needs. 'FetchConfig'
-- represent the collection of all such functions
data FetchConfig m (kvs :: [(Type, Type)]) where
  FetchNil :: FetchConfig m '[]
  (:&:) :: (k -> m v) -> FetchConfig m kvs -> FetchConfig m ('(k, v) ': kvs)

infixr 5 :&:

-- | Constraint indicating that m Fetches all the key-value pairs kvs
type family FetchesAll (kvs :: [(Type, Type)]) (m :: Type -> Type) :: Constraint where
  FetchesAll '[] m = ()
  FetchesAll ('(k, v) ': kvs) m = (FetchesAll kvs m, Fetches k v m)

-- | Type class that encapsulates the property that m' actions can be
-- lifted to m We demand that 'fetchLift' is a natural transformation,
-- which seems reasonable, and is probably true by parametricity.
-- However, it doesn't seem reasonable to demand it to be a natural
-- transformation of 'Applicative' or 'Monad', because we might be
-- lifting 'IO' into 'Concurrently IO', etc. It seems reasonable to
-- demand that 'pure' and 'return' are invariant though, and is
-- probably true by parametricity?
class LiftFetch m' m where
  liftFetch :: m' a -> m a

-- | A transformer that makes @m@ an instance of @'Fetches' m k v@ for all @(k,v)@
-- in @kvs@. This is based on @'ReaderT' ('FetchConfig' m kvs)@, from which we take
-- the Functor, Applicative, and MonadInstances.
newtype FetchT kvs m a = FetchT {runFetchTRaw :: ReaderT (FetchConfig m kvs) m a}
  deriving newtype (Functor, Applicative, Monad, MonadIO)
  deriving anyclass (HasForce)

-- | Runs a 'FetchT' given a 'FetchConfig'.
runFetchT :: FetchT kvs m a -> FetchConfig m kvs -> m a
runFetchT = runReaderT . runFetchTRaw

instance HasKeyVal k v kvs => Fetches k v (FetchT kvs m) where
  fetch key = FetchT . ReaderT $ \cfg -> go cfg keyValElem key
    where
      go :: FetchConfig m kvs' -> KeyValElem k' v' kvs' -> k' -> m v'
      go (get :&: _) Here k        = get k
      go (_ :&: cfg') (There e') k = go cfg' e' k
      go FetchNil _ _              = error "absurd"

instance LiftFetch m (FetchT kvs m) where
  liftFetch = FetchT . ReaderT . const

newtype ComputeDependencies kvs a = ComputeDependencies (Const [Sum (Keys kvs)] a)
  deriving newtype (Functor, Applicative)

instance HasKeyVal k v kvs => Fetches k v (ComputeDependencies kvs) where
  fetch key = ComputeDependencies . Const $ [toSumKeys @k @v @kvs keyValElem key]

instance HasForce (ComputeDependencies kvs) where
  forceM = id

instance LiftFetch m (ComputeDependencies kvs) where
  liftFetch _ = ComputeDependencies $ Const []

dependencies' :: ComputeDependencies kvs a -> [Sum (Keys kvs)]
dependencies' (ComputeDependencies (Const deps)) = deps

dependencies :: forall kvs ks a. (DropVoid (Sum (Keys kvs)) ks) => ComputeDependencies kvs a -> [ks]
dependencies = map dropVoid . dependencies'

