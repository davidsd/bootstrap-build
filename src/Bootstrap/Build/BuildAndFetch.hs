{-# LANGUAGE DataKinds #-}

module Bootstrap.Build.BuildAndFetch where

import Bootstrap.Build.BuildLink (BuildLink (..), SomeBuildChain,
                                  buildSomeChain, (<:<))
import Bootstrap.Build.Fetches   (ComputeDependencies, FetchConfig, FetchesAll,
                                  dependencies)
import Bootstrap.Build.HasForce  (HasForce)
import Bootstrap.Build.MemoFetch (MemoFetchAll, runMemoFetchT)
import Bootstrap.Build.TypeUtils (DropVoid, Keys, Sum)
import Data.Kind                 (Type)

-- | Build all the dependencies for an object and then create
-- it. (Need a better name for this function.)
buildAndFetch
  :: forall (kvs :: [(Type,Type)]) ks m r .
     ( DropVoid (Sum (Keys kvs)) ks
     , Ord ks
     , FetchesAll kvs (ComputeDependencies kvs)
     , MemoFetchAll kvs m
     , Monad m
     )
  => SomeBuildChain m ks
  -> FetchConfig m kvs
  -> (forall f . (Applicative f, HasForce f, FetchesAll kvs f) => f r)
  -> m r
buildAndFetch depBuildChain fetchConfig fetchResult = do
  buildSomeChain (link <:< depBuildChain) ()
  runMemoFetchT fetchResult fetchConfig
  where
    link = BuildLink
      { buildDeps    = const (dependencies (fetchResult :: ComputeDependencies kvs r))
      , checkCreated = const (pure False)
      , buildAll     = const (pure ())
      }
