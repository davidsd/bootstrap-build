{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE GADTs        #-}
{-# LANGUAGE TypeFamilies #-}

module Bootstrap.Build.BuildLink where

import Bootstrap.Build.FChain    (FChain (..), forgetFChain, mapFChain)
import Bootstrap.Build.FList     (FList (..), forgetF, zipWithF)
import Bootstrap.Build.Tree      (Dependencies (..), PredicateM (..),
                                  mergeTreeHorizontal, mkDependencyTree,
                                  prunePredicateM)
import Bootstrap.Build.TypeUtils (All)
import Data.DList                (DList)
import Data.DList                qualified as DList
import Data.Either               (lefts, rights)
import Data.Foldable             qualified as Foldable
import Data.Functor.Const        (Const (..))
import Data.Kind                 (Type)
import Data.Set                  (Set)
import Data.Set                  qualified as Set
import Data.Void                 (Void)

-- | Information about building a type 'k' with dependencies of type
-- 'd', in the 'm' monad.
data BuildLink m k d = BuildLink
  { -- | A list of dependency keys for a given key. All dependencies
    -- will be built before attempting 'buildAll'.
    buildDeps    :: k -> [d]
    -- | A monadic action that checks whether the given key has
    -- already been created. For example, checking for the existence
    -- of certain files.
  , checkCreated :: k -> m Bool
    -- | A monadic action for building a Set of keys. This action can
    -- make batch computations, run concurrently, or whatever it likes
    -- to build the Set as efficiently as possible.
  , buildAll     :: Set k -> m ()
  } deriving (Functor)

-- | Convenience type for a chain of 'BuildLink's
type BuildChain m = FChain (BuildLink m)

-- | A BuildLink, together with a proof of Ord k.
data OrdBuildLink m k d where
  MkOrdBuildLink :: Ord k => BuildLink m k d -> OrdBuildLink m k d

-- | A chain of OrdBuildLink's
type OrdBuildChain m = FChain (OrdBuildLink m)

-- | Decorate a BuildChain with Ord proofs for each key
ordBuildChain :: All Ord ks => BuildChain m ks -> OrdBuildChain m ks
ordBuildChain (link :< c)      = MkOrdBuildLink link :< ordBuildChain c
ordBuildChain (FChainEnd link) = FChainEnd (MkOrdBuildLink link)

-- | A BuildChain for which we only know the head key
data SomeBuildChain m k = forall (ks :: [Type]) . (Ord k, All Ord ks) => SomeBuildChain (BuildChain m (k ': ks))

-- | Convenience function for the end of a 'BuildChain'
noDeps :: BuildLink m k Void -> BuildChain m '[k]
noDeps = FChainEnd

-- | A monadic action for building a list of keys.
data BuildAll m k where
  BuildAll :: Ord k => (Set k -> m ()) -> BuildAll m k

-- | Build 'key' using the given 'BuildChain'
build :: (Monad m, Ord k, All Ord ks) => BuildChain m (k ': ks) -> k -> m ()
build buildChain key = do
  maybeBuildTree <- prunePredicateM checkCreatedFList (mkDependencyTree dependencyChain key)
  case maybeBuildTree of
    Nothing -> return ()
    Just buildTree ->
      sequence_ $
      reverse $
      forgetF $
      zipWithF buildDList (mkBuildAllFList buildChain) $
      mergeTreeHorizontal dummyFList buildTree
  where
    buildDList :: BuildAll m k -> DList k -> Const (m ()) k
    buildDList (BuildAll f) dList = Const (f (Set.fromList (DList.toList dList)))

    dependencyChain = mapFChain (Dependencies . buildDeps) buildChain
    checkCreatedFList = forgetFChain (PredicateM . checkCreated) buildChain
    dummyFList = forgetFChain (Const . const ()) buildChain

mkBuildAllFList :: All Ord ks => BuildChain m ks -> FList (BuildAll m) ks
mkBuildAllFList =
  forgetFChain (\(MkOrdBuildLink link) -> BuildAll (buildAll link)) .
  ordBuildChain

buildSomeChain :: (Monad m, Ord k) => SomeBuildChain m k -> k -> m ()
buildSomeChain (SomeBuildChain c) k = build c k

-- | A cons operation for SomeBuildChain
(<:<) :: (Ord k, Ord d) => BuildLink m k d -> SomeBuildChain m d -> SomeBuildChain m k
link <:< someChain = case someChain of
  SomeBuildChain depChain -> SomeBuildChain (link :< depChain)

infixr 4 <:<

-- | Given a 'BuildLink' for 'k1' with dependency type 'd1' and
-- another for 'k2' with dependency type 'd2', we have a 'BuildLink'
-- for 'Either k1 k2', where the dependency type is the sum of 'd1'
-- and 'd2', i.e. 'Either d1 d2'. The only non-canonical input needed
-- is to specify how to combine the monadic actions that create the
-- keys. This is passed in as an argument.
addBuildLink
  :: (Ord k2, Ord k1)
  => (m () -> m () -> m ()) -- ^ Combine a pair of monadic actions
                            -- (e.g. sequentially or in parallel)
  -> BuildLink m k1 d1
  -> BuildLink m k2 d2
  -> BuildLink m (Either k1 k2) (Either d1 d2)
addBuildLink doBoth cfg1 cfg2 = BuildLink
  { buildDeps    = either (map Left . buildDeps cfg1) (map Right . buildDeps cfg2)
  , checkCreated = either (checkCreated cfg1) (checkCreated cfg2)
  , buildAll     = \keys -> doBoth (buildAll cfg1 (setLefts keys)) (buildAll cfg2 (setRights keys))
  }
  where
    setLefts = Set.fromList . lefts . Set.toList
    setRights = Set.fromList . rights . Set.toList

-- | A zero with respect to 'addBuildLink'.
zeroBuildLink :: Applicative m => BuildLink m a Void
zeroBuildLink = BuildLink
  { buildDeps    = const []
  , checkCreated = const (pure True)
  , buildAll     = const (pure ())
  }

-- | A type family for zipping type level lists. In the usual zipWith,
-- the length of the result is the minimum of the two argument
-- lengths. However, for types, there exists a zero type 'Void'. We
-- pad the smaller list with 'Void' so that the result length is the
-- maximum of the two argument lengths.
type family ZipWithVoid f lst lst' where
  ZipWithVoid f '[]       '[]       = '[]
  ZipWithVoid f '[]       (n ': ns) = f Void n ': ZipWithVoid f '[] ns
  ZipWithVoid f (l ': ls) '[]       = f l Void ': ZipWithVoid f ls '[]
  ZipWithVoid f (l ': ls) (n ': ns) = f l n    ': ZipWithVoid f ls ns

-- | Add two build chains. If we imagine that each chain is oriented
-- vertically, this operation places them side-by-side, so that the
-- dependencies at the n-th levels line up and are built in lockstep.
addBuildChain
  :: forall ks ns m . (All Ord ks, All Ord ns, Applicative m)
  => (m () -> m () -> m ())
  -> BuildChain m ks
  -> BuildChain m ns
  -> BuildChain m (ZipWithVoid Either ks ns)
addBuildChain doBoth c1 c2 = go (ordBuildChain c1) (ordBuildChain c2)
  where
    go
      :: forall ks' ns' .
         FChain (OrdBuildLink m) ks'
      -> FChain (OrdBuildLink m) ns'
      -> BuildChain m (ZipWithVoid Either ks' ns')
    go (MkOrdBuildLink link1 :< bc1) (MkOrdBuildLink link2 :< bc2) =
      addBuildLink doBoth link1 link2 :< go bc1 bc2
    go (MkOrdBuildLink link1 :< bc1) (FChainEnd (MkOrdBuildLink link2)) =
      addBuildLink doBoth link1 link2 :< go bc1 (FChainEnd (MkOrdBuildLink zeroBuildLink))
    go (FChainEnd (MkOrdBuildLink link1)) (MkOrdBuildLink link2 :< bc2) =
      addBuildLink doBoth link1 link2 :< go (FChainEnd (MkOrdBuildLink zeroBuildLink)) bc2
    go (FChainEnd (MkOrdBuildLink link1)) (FChainEnd (MkOrdBuildLink link2)) =
      FChainEnd ((addBuildLink doBoth link1 link2) { buildDeps = const [] })

-- | Build a collection of keys
manyBuildLink :: (Foldable f, Applicative m) => BuildLink m (f a) a
manyBuildLink = BuildLink
  { buildDeps    = Foldable.toList
  , checkCreated = const (pure False)
  , buildAll     = const (pure ())
  }
