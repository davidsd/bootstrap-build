{-# LANGUAGE DataKinds               #-}
{-# LANGUAGE TypeFamilies            #-}
{-# LANGUAGE UndecidableInstances    #-}
{-# LANGUAGE UndecidableSuperClasses #-}

module Bootstrap.Build.TypeUtils where

import Data.Kind (Constraint, Type)
import Data.Void (Void)

-- | Type family used to implement All
type family AllF (c :: t -> Constraint) (ks :: [t]) :: Constraint where
  AllF c '[] = ()
  AllF c (k ': ks) = (c k, AllF c ks)

-- | Expresses that all elements of ks satisfy the constraint c This
-- is a class newtype, which unlike a type family can appear in
-- instance heads. See this video for an explanation of this trick:
-- 
-- https://www.youtube.com/watch?v=UkkKyobdzTU
-- 
-- Note that this needs UndecidableInstances and
-- UndecidableSuperClasses, see the example code here:
--
-- https://github.com/well-typed/unfolder/blob/main/episode006-computing-type-class-dicts/app/Main.hs
--
class    AllF c ks => All c ks
instance AllF c ks => All c ks

-- | Keys = Map Fst. However, because Fst cannot be unsaturated in the
-- current version of Haskell, we have to write a specialized version.
type family Keys (kvs :: [(Type, Type)]) where
  Keys '[] = '[]
  Keys ('(k, _) ': kvs) = k ': Keys kvs

-- | A type-level sum of the given list of types, using Either as the
-- addition operation.
--
-- > Sum [k1,k2,...kn] = Either k1 (Either k2 (... (Either kn Void)))
type family Sum (ks :: [Type]) where
  Sum '[] = Void
  Sum (k ': ks) = Either k (Sum ks)

-- | Type-level version of map
type family Map (f :: Type -> Type) (ks :: [Type]) :: [Type] where
  Map f '[] = '[]
  Map f (k ': ks) = f k ': Map f ks

-- | The same as Sum, but where the last element is (Either kn-1 kn)
--
-- > SumDropVoid [k1,k2,...kn] = Either k1 (Either k2 (... (Either kn-1 kn)))
--
type family SumDropVoid (ks :: [Type]) :: Type where
  SumDropVoid '[] = Void
  SumDropVoid '[k] = k
  SumDropVoid (k ': ks) = Either k (SumDropVoid ks)

-- | It is annoying that 'Sum [k] = Either k Void', so we define a
-- typeclass to drop the Void at the end. DropVoid a b is a constraint
-- that says that the type b is obtained by dropping the Void at the
-- end of the type a. It is witnessed by a function 'a -> b' which
-- should be an isomorphism (though I don't know how to impose this in
-- the type system).
--
-- WARNING: Bad things could probably happen if the user defines
-- orphan instances of this typeclass.
class DropVoid a b where
  dropVoid :: a -> b

instance DropVoid Void Void where
  dropVoid = id

instance DropVoid (Either a Void) a where
  dropVoid (Left x) = x
  dropVoid _        = error "absurd"

instance DropVoid a b => DropVoid (Either c a) (Either c b) where
  dropVoid (Left x)  = Left x
  dropVoid (Right x) = Right (dropVoid x)
