{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE GADTs        #-}
{-# LANGUAGE TypeFamilies #-}

module Bootstrap.Build.FChain where

import Bootstrap.Build.FList (FList (..))
import Data.Kind             (Type)
import Data.Void             (Void)

-- | A heterogeneous "linked-list", where the elements have types 'f
-- x1 x2, f x2 x3, f x3 x4, ..., f xn Void', where 'f' is a
-- two-argument type constructor.
data FChain f (xs :: [Type]) where
  -- | FChainEnd is the 1-element chain. TODO: Can all this be made to
  -- work using a 0-element chain, as in FList?
  FChainEnd :: f x Void -> FChain f '[x]
  -- | The cons operation for FChain
  (:<) :: f x y -> FChain f (y ': ys) -> FChain f (x ': y ': ys)

infixr 5 :<

-- | Map a 2-argument natural transformation over an FChain
mapFChain :: (forall a b . f a b -> g a b) -> FChain f xs -> FChain g xs
mapFChain f (FChainEnd x) = FChainEnd (f x)
mapFChain f (x :< xs)     = f x :< mapFChain f xs

-- | Forget the "linked" structure of an FChain, turning it into an
-- FList by applying a forgetful transformation to each element. TODO:
-- Is there a canonical such transformation, perhaps involving 'Const'?
forgetFChain :: (forall a b . f a b -> g a) -> FChain f xs -> FList g xs
forgetFChain f (FChainEnd x) = f x ::: FNil
forgetFChain f (x :< xs)     = f x ::: forgetFChain f xs
