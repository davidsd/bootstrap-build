{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE GADTs        #-}
{-# LANGUAGE TypeFamilies #-}

module Bootstrap.Build.Tree where

import Bootstrap.Build.FChain (FChain (..))
import Bootstrap.Build.FList  (FList (..), mapF, zipWithF)
import Control.Monad.Extra    (mapMaybeM)
import Data.DList             (DList)
import Data.DList             qualified as DList
import Data.Kind              (Type)

-- | A heterogenous Rose tree whose argument gives the types of
-- elements at each level.
data HRoseTree (ks :: [Type]) where
  HRTNode :: k -> [HRoseTree ks] -> HRoseTree (k ': ks)

-- | A map from a key to a list of dependencies
newtype Dependencies k d = Dependencies (k -> [d])

-- | Make a tree of dependencies for the given 'key' using the given
-- chain of Dependencies.
mkDependencyTree :: FChain Dependencies (k ': ks) -> k -> HRoseTree (k ': ks)
mkDependencyTree (FChainEnd _) key = HRTNode key []
mkDependencyTree (Dependencies mkDeps :< bs) key =
  HRTNode key (map (mkDependencyTree bs) (mkDeps key))

-- | A monadic predicate -- i.e. a monadic function that returns a
-- 'Bool'.
newtype PredicateM m k = PredicateM (k -> m Bool)

-- | Prune the tree by applying a monadic predicate to each level. If
-- the predicate returns true, that part of the tree is removed
-- (together with its children).
prunePredicateM :: Monad m => FList (PredicateM m) ks -> HRoseTree ks -> m (Maybe (HRoseTree ks))
prunePredicateM FNil t = pure (Just t)
prunePredicateM (PredicateM p ::: ps) (HRTNode key deps) = do
  shouldPrune <- p key
  if shouldPrune
    then return Nothing
    else Just . HRTNode key <$> mapMaybeM (prunePredicateM ps) deps

-- | Merge an 'HRoseTree' horizontally, collecting elements at each
-- level into a 'DList'. We use a 'DList' because it offers an
-- efficient append operation.
--
-- Annoyingly, we have to pass in a dummy 'FList' from which one can
-- build an 'FList' of empty 'DLists'. TODO: Can one construct this
-- directly from the 'HRoseTree'? I couldn't figure it out.
mergeTreeHorizontal
  :: FList f ks
  -> HRoseTree ks
  -> FList DList ks
mergeTreeHorizontal FNil _ = FNil
mergeTreeHorizontal (_ ::: bs) (HRTNode key deps) =
  DList.singleton key :::
  foldr (zipWithF (<>) . mergeTreeHorizontal bs) (emptyLists bs) deps
  where
    emptyLists :: FList f ks -> FList DList ks
    emptyLists = mapF (const mempty)
