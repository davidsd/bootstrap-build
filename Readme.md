bootstrap-build
---------------

A library for building objects with a chain of dependencies.

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/bootstrap-build/)
